/*
Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.
*/

// Using promises and the `fetch` library, do the following.
// If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.
// Usage of the path libary is recommended

// 1. Fetch all the users
const fs = require("fs/promises");
const path = require("path");

function getUsers(url) {
  return new Promise((resolve, reject) => {
    fetch(url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("404 Error");
        }
      })

      .then((data) => {
        resolve(data);
        fs.writeFile(path.join(__dirname, "output1.json"), JSON.stringify(data))
          .then(() => {
            console.log("Output 1 created");
          })
          .catch((err) => {
            reject(err);
            console.log(err);
          });
      })

      .catch((err) => {
        console.log(1);
        console.log(err);
      });
  });
}

// getUsers("https://jsonplaceholder.typicode.com/users");

// 2. Fetch all the todos
function getTodos(url) {
  return fetch(url)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("404 Error");
      }
    })

    .then((data) => {
      fs.writeFile(path.join(__dirname, "output2.json"), JSON.stringify(data))
        .then(() => {
          console.log("Output 2 created");
        })
        .catch((err) => {
          console.log(err);
        });
    })

    .catch((err) => {
      console.log(err);
    });
}

// getTodos("https://jsonplaceholder.typicode.com/todos");

// 3. Use the promise chain and fetch the users first and then the todos.
function getUsersWithTodos(userUrl, todoUrl) {
  getUsers(userUrl)
    .then(() => {
      getTodos(todoUrl);
    })

    .catch((err) => {
      console.log(err);
    });
}

// getUsersWithTodos(
//   "https://jsonplaceholder.typicode.com/users",
//   "https://jsonplaceholder.typicode.com/todos"
// );

// 4. Use the promise chain and fetch the users first and then all the details for each user.
function getUserDetails(url, userId) {
  getUsers(url).then((data) => {
    fs.writeFile(
      path.join(__dirname, "output4.json"),
      JSON.stringify(
        data.filter((user) => {
          return user.id === userId;
        })
      )
    )
      .then(() => {
        console.log("Output 4 created");
      })
      .catch((err) => {
        console.log(err);
      });
  });
}

getUserDetails("https://jsonplaceholder.typicode.com/users", 2);
// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo
